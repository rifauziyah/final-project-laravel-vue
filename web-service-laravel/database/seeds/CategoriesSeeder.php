<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['name' => 'Biography']);
        Category::create(['name' => 'Comic']);
        Category::create(['name' => 'Fiction']);
        Category::create(['name' => 'History']);
        Category::create(['name' => 'Horror']);
        Category::create(['name' => 'Humor']);
        Category::create(['name' => 'Romance']);
        Category::create(['name' => 'Science']);
        Category::create(['name' => 'Self Help']);
    }
}
