<?php

use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create(['name' => 'admin']);
        $member = Role::create(['name' => 'member']);

        User::create([
            'username'          => 'admin',
            'email'             => 'admin@admin.com',
            'password'          => Hash::make('123456'),
            'email_verified_at' => Carbon::now(),
            'role_id'           => $admin->id
        ]);

        User::create([
            'username'          => 'rifauziyah',
            'email'             => 'rifauziyah@gmail.com',
            'password'          => Hash::make('123456'),
            'email_verified_at' => Carbon::now(),
            'role_id'           => $member->id
        ]);

    }
}
