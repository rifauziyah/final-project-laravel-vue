<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use App\Author;
use App\Category;
use Faker\Generator as Faker;

$factory->define(Author::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
    ];
});

$factory->define(Book::class, function (Faker $faker) {
    return [
        'title'         => $faker->sentence(3),
        'synopsis'      => $faker->paragraph($nbSentences = 10, $variableNbSentences = true),
        'author_id'     => Author::inRandomOrder()->first()->id,
        'category_id'   => Category::inRandomOrder()->first()->id,
        'year'          => $faker->numberBetween(1980, 2021),
    ];
});