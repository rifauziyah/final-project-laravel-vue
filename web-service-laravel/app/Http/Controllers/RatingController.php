<?php

namespace App\Http\Controllers;

use App\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RatingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except('index', 'show');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ratings = Rating::latest()->get();

        //respons JSON
        return response()->json([
            'success' => true,
            'message' => 'Daftar rating berhasil ditampilkan',
            'data'    => $ratings
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'book_id' => 'required',
            'review'  => 'required',
            'point'   => 'required|numeric|min:0|max:10',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //simpan data ke database
        $rating = Rating::create([
            'book_id' => $request->book_id,
            'review'  => $request->review,
            'point'   => $request->point,
        ]);

        //berhasil menyimpan ke database
        if($rating) {
            return response()->json([
                'success' => true,
                'message' => 'Data rating berhasil dibuat',
                'data'    => $rating
            ], 201);
        }

        //gagal menyimpan ke database
        return response()->json([
            'success' => false,
            'message' => 'Data rating gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find post by ID
        $rating = Rating::with('user')->where('book_id', $id)->get();

        if($rating)
        {
            //respons JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail rating berhasil ditampilkan',
                'data'    => $rating
            ], 200);
        }

        //data post tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Rating tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'book_id' => 'required',
            'review'  => 'required',
            'point'   => 'required|numeric|min:0|max:10',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $rating = Rating::find($id);

        if($rating) {

            $user = auth()->user();

            if ($rating->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data rating bukan milik user login',
                ], 403);
            }

            //update rating
            $rating->update([
                'book_id' => $request->book_id,
                'review'  => $request->review,
                'point'   => $request->point,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Rating berhasil diperbaharui',
                'data'    => $rating  
            ], 200);
        }

        //data tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Data rating tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rating = Rating::find($id);

        if($rating) {

            $user = auth()->user();

            if ($rating->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data rating bukan milik user login',
                ], 403);
            }

            // delete rating
            $rating->delete();

            return response()->json([
                'success' => true,
                'message' => 'Rating berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Data rating tidak ditemukan',
        ], 404);
    }
}
