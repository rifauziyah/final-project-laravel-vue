<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UploadCoverImageController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api', 'admin']);
    }

    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'cover_image' => 'required|mimes:jpeg,png,jpg',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $book = Book::findOrFail($id);

        if ($request->hasFile('cover_image')) {

            $image = $request->file('cover_image');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = $book->id . "." . $image_extension;
            $image_folder = '/img/book/';
            $image_location =  $image_folder . $image_name;
    
            try {
              $image->move(public_path($image_folder) , $image_name);
    
              $book->update([
                'cover_image' => $image_location,
              ]);
    
            } catch (\Exception $e) {
              return response()->json([
                'response_code'    => '01',
                'response_message' => 'Cover image gagal diupload' ,
                'data'             => $book
              ], 200);
            }
            
            return response()->json([
                'success' => true,
                'message' => 'Cover image berhasil diupload',
                'data'    => $book  
            ], 200);
        }
    }
}
