<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthorController extends Controller
{
    
    public function __construct()
    {
        $this->middleware(['auth:api', 'admin'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authors = Author::all();

        return response()->json([
            'success' => true,
            'message' => 'Daftar penulis berhasil ditampilkan',
            'data'    => $authors
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //simpan data ke database
        $author = Author::create([
            'name' => $request->name
        ]);

        //berhasil menyimpan ke database
        if($author) {
            return response()->json([
                'success' => true,
                'message' => 'Data penulis berhasil dibuat',
                'data'    => $author
            ], 201);
        }

        //gagal menyimpan ke database
        return response()->json([
            'success' => false,
            'message' => 'Data penulis gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $author = Author::find($id);

        if($author)
        {
            return response()->json([
                'success' => true,
                'message' => 'Detail penulis berhasil ditampilkan',
                'data'    => $author
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Penulis tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $author = Author::find($id);
        
        if($author)
        {
            $author->update([
                'name' => $request->name
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data penulis berhasil diperbaharui',
                'data'    => $author
            ], 200);
        }

        //data tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Penulis tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author = Author::find($id);

        if($author) {

            $author->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data penulis berhasil dihapus',
            ], 200);
        }

        //data tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Penulis tidak ditemukan',
        ], 404);
    }
}
