<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api', 'admin'])->except('index', 'show');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return response()->json([
            'success' => true,
            'message' => 'Daftar kategori berhasil ditampilkan',
            'data'    => $categories
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //simpan data ke database
        $category = Category::create([
            'name' => $request->name
        ]);

        //berhasil menyimpan ke database
        if($category) {
            return response()->json([
                'success' => true,
                'message' => 'Data kategori berhasil dibuat',
                'data'    => $category
            ], 201);
        }

        //gagal menyimpan ke database
        return response()->json([
            'success' => false,
            'message' => 'Data kategori gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);

        if($category)
        {
            return response()->json([
                'success' => true,
                'message' => 'Detail kategori berhasil ditampilkan',
                'data'    => $category
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Kategori tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $category = Category::find($id);
        
        if($category)
        {
            $category->update([
                'name' => $request->name
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data kategori berhasil diperbaharui',
                'data'    => $category
            ], 200);
        }

        //data tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Kategori tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if($category) {

            $category->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data kategori berhasil dihapus',
            ], 200);
        }

        //data tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Kategori tidak ditemukan',
        ], 404);
    }
}
