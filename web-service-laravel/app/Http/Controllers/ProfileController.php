<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user_id = auth()->user()->id;
        $profile = Profile::where('user_id', $user_id)->first();

        return response()->json([
            'success' => true,
            'message' => 'Profile user berhasil ditampilkan',
            'data'    => $profile
        ], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'full_name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //simpan data ke database
        $profile = Profile::create([
            'full_name' => $request->full_name,
            'bio'       => $request->bio,
        ]);

        //berhasil menyimpan ke database
        if($profile) {
            return response()->json([
                'success' => true,
                'message' => 'Data profil berhasil dibuat',
                'data'    => $profile
            ], 201);
        }

        //gagal menyimpan ke database
        return response()->json([
            'success' => false,
            'message' => 'Data profil gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'full_name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $profile = Profile::find($id);
        
        if($profile)
        {
            $profile->update([
                'full_name' => $request->full_name,
                'bio'       => $request->bio,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data profil berhasil diperbaharui',
                'data'    => $profile
            ], 200);
        }

        //data tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Profil tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
