<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api', 'admin'])->except('index', 'show');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::with(['author', 'category'])->latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'Daftar buku berhasil ditampilkan',
            'data'    => $books
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'title'       => 'required',
            'synopsis'    => 'required',
            'author_id'   => 'required',
            'category_id' => 'required',
            'year'        => 'required|digits:4|date_format:Y',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //simpan data ke database
        $book = Book::create([
            'title'       => $request->title,
            'synopsis'    => $request->synopsis,
            'author_id'   => $request->author_id,
            'category_id' => $request->category_id,
            'year'        => $request->year,
        ]);

        //berhasil menyimpan ke database
        if($book) {
            return response()->json([
                'success' => true,
                'message' => 'Data buku berhasil dibuat',
                'data'    => $book
            ], 201);
        }

        //gagal menyimpan ke database
        return response()->json([
            'success' => false,
            'message' => 'Data buku gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::with(['author', 'category'])->find($id);

        if($book)
        {
            return response()->json([
                'success' => true,
                'message' => 'Detail buku berhasil ditampilkan',
                'data'    => $book
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Buku tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'title'       => 'required',
            'synopsis'    => 'required',
            'author_id'   => 'required',
            'category_id' => 'required',
            'year'        => 'required|numeric|digits:4|max:' . (date('Y')),
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $book = Book::find($id);
        
        if($book)
        {
            $book->update([
                'title'       => $request->title,
                'synopsis'    => $request->synopsis,
                'author_id'   => $request->author_id,
                'category_id' => $request->category_id,
                'year'        => $request->year,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data buku berhasil diperbaharui',
                'data'    => $book
            ], 200);
        }

        //data tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Buku tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);

        if($book) {

            $book->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data buku berhasil dihapus',
            ], 200);
        }

        //data tidak ditemukan
        return response()->json([
            'success' => false,
            'message' => 'Buku tidak ditemukan',
        ], 404);
    }
}
