<?php

namespace App\Listeners;

use App\Mail\OtpCodeMail;
use App\Events\OtpCodeStored;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOtpCodeMail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpCodeStored  $event
     * @return void
     */
    public function handle(OtpCodeStored $event)
    {
        Mail::to($event->otpCode->user->email)
                ->send(new OtpCodeMail($event->otpCode, $event->newUserStatus));
    }
}
