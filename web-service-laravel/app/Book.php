<?php

namespace App;

use App\Author;
use App\Category;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['title', 'synopsis', 'author_id', 'category_id', 'cover_image', 'year'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    public function author(){
        return $this->belongsTo('App\Author');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function rating(){
        return $this->hasMany('App\Rating');
    }
}
