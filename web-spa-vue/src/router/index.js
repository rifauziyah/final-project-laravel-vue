import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/detail/:id',
    name: 'Detail',
    component: () => import('../views/Detail.vue'),
    props: true
  },
  {
    path: '/add',
    name: 'Add Book',
    component: () => import('../views/AddBook.vue'),
    meta: { requiresLogin: true }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach ((to, from, next) => {
  var isGuest = store.getters['auth/isGuest']
  if (to.matched.some(record => record.meta.requiresLogin) && !isGuest) {
    var user = store.getters['auth/user']
    if (to.path == '/add' && user.role.name == 'admin') {
      next()
    } else {
      alert('Anda bukan Admin!')
      next(false)
    }
  }
  else next()
})

export default router
