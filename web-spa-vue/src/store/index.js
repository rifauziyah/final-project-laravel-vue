import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import auth from './auth'
import snackbar from './snackbar'

const vuexPersist = new VuexPersist({
    key: 'bookreview',
    storage: localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [vuexPersist.plugin],
    modules: {
        auth,
        snackbar
    },
})