export default {
    namespaced: true,
    state: {
        status: false,
        text: '',
        color: 'success'
    },
    mutations: {
        set: (state, payload) => {
            state.status = payload.status
            state.text = payload.text
            state.color = payload.color
        }
    },
    actions: {
        set: ({ commit }, payload) => {
            commit('set', payload)
        }
    },
    getters: {
        status: state => state.status,
        text: state => state.text,
        color: state => state.color
    }
}