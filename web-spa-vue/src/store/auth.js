export default {
    namespaced: true,
    state: {
        domain: 'http://localhost:8000',
        user: {},
        token: '',
    },
    getters: {
        domain: state => state.domain,
        user: state => state.user,
        token: state => state.token,
        isGuest: state => Object.keys(state.token).length === 0
    },
    mutations: {
        login(state, data) {
            state.user = data.user
            state.token = data.token
        },
        logout(state) {
            state.user = {}
            state.token = ''
        }
    },
    actions: {
        login ({commit}, data) {
            commit('login', data)
        },
        logout ({commit}) {
            commit('logout')
        }
    }
}