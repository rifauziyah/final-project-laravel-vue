# Final Project

## Kelompok 10

## Anggota Kelompok

-   Rifa Fauziyah
-   Zidan Aqila Muhammad

## ERD

![ERD](erd_book_review_app.png?raw=true "Book Review App ERD")

## Link

- [Dokumentasi POSTMAN](https://documenter.getpostman.com/view/18402318/UVR4MUxb)
- [Screenshot](https://drive.google.com/drive/folders/14Z-RWJw7UP9DTOuCh2SAdxWsvWds9jfo?usp=sharing)